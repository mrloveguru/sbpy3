# PYTHON3
SELF BOT PYTHON3.
------
-
Cara Install Self Bot Di Termux :
------
- Ketik -> `apt update && apt upgrade`
- Ketik -> `pkg update && pkg upgrade`
- Ketik -> `pkg install git`
- Ketik -> `pkg install python`
- Ketik -> `pkg install python-pip`
- Ketik -> `pip3 install rsa`
- Ketik -> `pip3 install thrift`
- Ketik -> `pip3 install requests`
- Ketik -> `pip3 install bs4`
- Ketik -> `pip3 install gtts`
- Ketik -> `pip3 install pytz`
- Ketik -> `pip3 install humanfriendly`
- Ketik -> `pip3 install googletrans`
- Ketik -> `git clone https://gitlab.com/zelebez6969/sbpy3`
- Ketik -> `cd sbpy3`
- Ketik -> `python3 sb.py`

Cara Menjalankan Bot Kembali :
------
- Ketik -> `cd sbpy3`
- Ketik -> `python3 sb.py`


Credit By @zelebez6969
------
Thx To :
------
- `LINE-TCR TEAM`
- `HELLO-WORLD`
